﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.Excercises.Apps.Services.Helpers;
using WooliesX.Excercises.Apps.Services.Models;

namespace WooliesX.Excercises.Apps.Services
{
    public class TrolleyService : ITrolleyService
    {
        private readonly IWooliesXApiHelper _wooliesXApiHelper;

        public TrolleyService
        (
            IWooliesXApiHelper wooliesXApiHelper
        )
        {
            _wooliesXApiHelper = wooliesXApiHelper;
        }

        public async Task<decimal> GetTrolleyTotalAsync(string requestBaseUrl, string token, TrolleyModel model)
        {
            return await _wooliesXApiHelper.PostTrolleyCalculatorAsync(requestBaseUrl, token, model);
        }

        public decimal GetTrolleyTotal2(TrolleyModel model)
        {
            if ((model.Quantities?.Count() ?? 0) == 0)
            {
                return 0;
            }

            var productsInTrolley = model.Quantities
                .Join(
                    model.Products,
                    x => x.Name,
                    x => x.Name,
                    (productQuantity, productPrice) => new ProductModel
                    {
                        Name = productQuantity.Name,
                        Quantity = productQuantity.Quantity,
                        Price = productPrice.Price
                    })
                .ToList();
            
            var totals = new List<decimal>();
            
            foreach (var special in model.Specials)
            {
                var specialProductsInTrolley = special.Quantities
                    .Join(
                        productsInTrolley,
                        x => x.Name,
                        x => x.Name,
                        (specialProduct, productInTrolley) => new
                        {
                            Name = productInTrolley.Name,
                            Price = productInTrolley.Price,
                            QuantityInTrolley = productInTrolley.Quantity,
                            QuantityInSpecial = specialProduct.Quantity
                        })
                    .ToList();

                var areAllSpecialProductsInTrolley = specialProductsInTrolley.Count() == special.Quantities.Count();
                
                if (areAllSpecialProductsInTrolley)
                {
                    var specialCount = specialProductsInTrolley.Select(x => x.QuantityInSpecial == 0 
                        ? decimal.MaxValue 
                        : (int)x.QuantityInTrolley / (int)x.QuantityInSpecial
                        ).Min();

                    var specialTotal = specialCount * special.Total;

                    var remainingTotal = 0M;
                    foreach (var productInTrolley in productsInTrolley)
                    {
                        var specialQuantity = special.Quantities.FirstOrDefault(x => x.Name == productInTrolley.Name);
                        if (specialQuantity == null)
                        {
                            remainingTotal += productInTrolley.Quantity * productInTrolley.Price;
                        }
                        else
                        {
                            remainingTotal += (productInTrolley.Quantity - specialCount * specialQuantity.Quantity) * productInTrolley.Price;
                        }
                    }
                    
                    var total = specialTotal + remainingTotal;
                    
                    totals.Add(total);
                }
            }
            
            return totals.Min();
        }
    }
}
