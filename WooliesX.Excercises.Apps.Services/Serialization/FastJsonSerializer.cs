﻿using Newtonsoft.Json;
using System.IO;

namespace WooliesX.Excercises.Apps.Services.Serialization
{
    public static class FastJsonSerializer
    {
        public static T Deserialize<T>(Stream stream)
        {
            using (var streamReader = new StreamReader(stream))
            {
                using (var jsonTextReader = new JsonTextReader(streamReader))
                {
                    var serializer = new JsonSerializer();

                    // read the json from a stream
                    // json size doesn't matter because only a small piece is read at a time from the HTTP request
                    var result = serializer.Deserialize<T>(jsonTextReader);

                    return result;
                }
            }

        }
    }
}
