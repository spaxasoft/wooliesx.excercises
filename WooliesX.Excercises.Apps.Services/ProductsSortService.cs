﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.Excercises.Apps.Services.Helpers;
using WooliesX.Excercises.Apps.Services.Models;

namespace WooliesX.Excercises.Apps.Services
{
    public class ProductsSortService : IProductsSortService
    {
        private readonly IWooliesXApiHelper _wooliesXApiHelper;

        public string[] SupportedSortOptions
        {
            get
            {
                return new[]
                {
                    "low",
                    "high",
                    "ascending",
                    "descending",
                    "recommended"
                };
            }
        }


        public ProductsSortService
        (
            IWooliesXApiHelper wooliesXApiHelper
        )
        {
            _wooliesXApiHelper = wooliesXApiHelper;
        }

        public async Task<IEnumerable<ProductModel>> GetSortedProductsAsync(string requestBaseUrl, string token, string sortOption)
        {
            var products = await _wooliesXApiHelper.GetProductsAsync(requestBaseUrl, token);

            var shoppersHistory = sortOption == "recommended"
                ? await _wooliesXApiHelper.GetShoppersHistoryAsync(requestBaseUrl, token)
                : null;

            var result = SortProducts(products, shoppersHistory, sortOption);

            return result;
        }

        private IEnumerable<ProductModel> SortProducts
        (
            IEnumerable<ProductModel> products,
            IEnumerable<ShopperHistoryModel> shoppersHistory,
            string sortOptions)
        {
            switch (sortOptions)
            {
                case "low":
                    return products.OrderBy(x => x.Price);
                case "high":
                    return products.OrderByDescending(x => x.Price);
                case "ascending":
                    return products.OrderBy(x => x.Name);
                case "descending":
                    return products.OrderByDescending(x => x.Name);
                case "recommended":
                    return SortProductsOnPopularity(products, shoppersHistory);
                default:
                    return products;
            }
        }

        private IEnumerable<ProductModel> SortProductsOnPopularity(IEnumerable<ProductModel> products, IEnumerable<ShopperHistoryModel> shoppersHistory)
        {
            var shoppersHistoryProducts = shoppersHistory.SelectMany(x => x.Products);

            var productsSorted = products
                .GroupJoin(shoppersHistoryProducts, x => x.Name, x => x.Name, (x, y) => new { Product = x, TotalQuantity = y.Sum(z => z.Quantity) })
                .OrderByDescending(x => x.TotalQuantity)
                .Select(x => x.Product)
                .ToList();

            return productsSorted;
        }

    }
}
