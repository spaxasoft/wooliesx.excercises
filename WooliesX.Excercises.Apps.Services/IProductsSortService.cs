﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesX.Excercises.Apps.Services.Models;

namespace WooliesX.Excercises.Apps.Services
{
    public interface IProductsSortService
    {
        string[] SupportedSortOptions { get; }

        Task<IEnumerable<ProductModel>> GetSortedProductsAsync(string requestBaseUrl, string token, string sortOption);
    }
}
