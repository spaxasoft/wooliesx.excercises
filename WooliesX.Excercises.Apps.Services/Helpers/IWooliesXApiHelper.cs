﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesX.Excercises.Apps.Services.Models;

namespace WooliesX.Excercises.Apps.Services.Helpers
{
    public interface IWooliesXApiHelper
    {
        Task<IEnumerable<ProductModel>> GetProductsAsync(string requestBaseUrl, string token);
        Task<IEnumerable<ShopperHistoryModel>> GetShoppersHistoryAsync(string requestBaseUrl, string token);
        Task<decimal> PostTrolleyCalculatorAsync(string requestBaseUrl, string token, TrolleyModel model);
    }
}
