﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using WooliesX.Excercises.Apps.Services.Models;
using WooliesX.Excercises.Apps.Services.Serialization;

namespace WooliesX.Excercises.Apps.Services.Helpers
{
    public class WooliesXApiHelper : IWooliesXApiHelper
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public WooliesXApiHelper
        (
            IHttpClientFactory httpClientFactory
        )
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<IEnumerable<ProductModel>> GetProductsAsync(string requestBaseUrl, string token)
        {
            var requestUrl = $"{requestBaseUrl}/products?token={token}";

            var products = await GetApiAsync<IEnumerable<ProductModel>>(requestUrl);

            return products;
        }

        public async Task<IEnumerable<ShopperHistoryModel>> GetShoppersHistoryAsync(string requestBaseUrl, string token)
        {
            var requestUrl = $"{requestBaseUrl}/shopperHistory?token={token}";

            var shoppersHistory = await GetApiAsync<IEnumerable<ShopperHistoryModel>>(requestUrl);

            return shoppersHistory;
        }

        public async Task<decimal> PostTrolleyCalculatorAsync(string requestBaseUrl, string token, TrolleyModel model)
        {
            var requestUrl = $"{requestBaseUrl}/trolleyCalculator?token={token}";

            var response = await PostApiAsync<TrolleyModel, decimal>(requestUrl, model);

            return response;
        }

        private async Task<T> GetApiAsync<T>(string requestUrl)
        {
            using (var httpClient = _httpClientFactory.CreateClient())
            {
                using (var response = await httpClient.GetAsync(requestUrl))
                {
                    return await GetResponseAs<T>(response);
                }
            }
        }

        private async Task<TResponse> PostApiAsync<TModel, TResponse>(string requestUrl, TModel model)
        {
            using (var httpClient = _httpClientFactory.CreateClient())
            {
                using (var httpContent = JsonContent.Create(model))
                {
                    using (var response = await httpClient.PostAsync(requestUrl, httpContent))
                    {
                        return await GetResponseAs<TResponse>(response);
                    }
                }
            }
        }

        private async Task<T> GetResponseAs<T>(HttpResponseMessage response)
        {
            response.EnsureSuccessStatusCode();

            using (var responseContent = await response.Content.ReadAsStreamAsync())
            {
                return FastJsonSerializer.Deserialize<T>(responseContent);
            }
        }
    }
}
