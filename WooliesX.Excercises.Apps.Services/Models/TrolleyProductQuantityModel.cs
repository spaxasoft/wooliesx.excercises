﻿namespace WooliesX.Excercises.Apps.Services.Models
{
    public class TrolleyProductQuantityModel
    {
        public string Name { get; set; }
        public decimal Quantity { get; set; }
    }
}
