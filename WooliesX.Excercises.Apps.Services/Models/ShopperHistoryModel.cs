﻿using System.Collections.Generic;

namespace WooliesX.Excercises.Apps.Services.Models
{
    public class ShopperHistoryModel
    {
        public int CustomerId { get; set; }
        public IEnumerable<ProductModel> Products { get; set; }
    }
}
