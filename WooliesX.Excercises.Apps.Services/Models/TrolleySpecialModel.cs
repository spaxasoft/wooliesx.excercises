﻿using System.Collections.Generic;

namespace WooliesX.Excercises.Apps.Services.Models
{
    public class TrolleySpecialModel
    {
        public IEnumerable<TrolleyProductQuantityModel> Quantities { get; set; }
        public decimal Total { get; set; }
    }
}
