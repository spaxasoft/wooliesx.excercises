﻿namespace WooliesX.Excercises.Apps.Services.Models
{
    public class TrolleyProductPriceModel
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
