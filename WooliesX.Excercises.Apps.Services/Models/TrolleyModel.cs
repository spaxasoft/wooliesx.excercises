﻿using System.Collections.Generic;

namespace WooliesX.Excercises.Apps.Services.Models
{
    public class TrolleyModel
    {
        public IEnumerable<TrolleyProductPriceModel> Products { get; set; }
        public IEnumerable<TrolleySpecialModel> Specials { get; set; }
        public IEnumerable<TrolleyProductQuantityModel> Quantities { get; set; }
    }
}
