﻿using System.Threading.Tasks;
using WooliesX.Excercises.Apps.Services.Models;

namespace WooliesX.Excercises.Apps.Services
{
    public interface ITrolleyService
    {
        Task<decimal> GetTrolleyTotalAsync(string requestBaseUrl, string token, TrolleyModel model);
        decimal GetTrolleyTotal2(TrolleyModel model);
    }
}
