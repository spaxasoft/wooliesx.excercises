﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using WooliesX.Excercises.Apps.Services;
using WooliesX.Excercises.Apps.Services.Helpers;

[assembly: FunctionsStartup(typeof(WooliesX.Excercises.Apps.AzFuncs.Startup))]

namespace WooliesX.Excercises.Apps.AzFuncs
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddHttpClient();

            builder.Services
                .AddTransient<IWooliesXApiHelper, WooliesXApiHelper>()
                .AddTransient<IProductsSortService, ProductsSortService>()
                .AddTransient<ITrolleyService, TrolleyService>();
        }
    }
}
