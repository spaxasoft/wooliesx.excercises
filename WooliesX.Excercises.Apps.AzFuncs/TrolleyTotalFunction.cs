using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net;
using WooliesX.Excercises.Apps.Services;
using WooliesX.Excercises.Apps.Services.Serialization;
using WooliesX.Excercises.Apps.Services.Models;
using System.Linq;
using System.Collections.Generic;

namespace WooliesX.Excercises.Apps.AzFuncs
{
    public class TrolleyTotalFunction
    {
        private readonly ITrolleyService _trolleyService;

        public TrolleyTotalFunction(ITrolleyService trolleyService)
        {
            _trolleyService = trolleyService;
        }

        [FunctionName("TrolleyTotal")]
        public async Task<IActionResult> Run
        (
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            ILogger log
        )
        {
            try
            {
                var model = FastJsonSerializer.Deserialize<TrolleyModel>(req.Body);

                if (model == null || model.Quantities == null)
                {
                    var errorResponse = "Invalid trolley request body";

                    log.LogError("Bad Request: {0}", errorResponse);

                    return new BadRequestObjectResult(errorResponse);
                }

                var requestBaseUrl = Environment.GetEnvironmentVariable("WooliesXApiBaseUrl");
                var token = Environment.GetEnvironmentVariable("Token");

                var trolleyTotal = await _trolleyService.GetTrolleyTotalAsync(requestBaseUrl, token, model);

                return new OkObjectResult(trolleyTotal);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "An error occured in /TrolleyTotal function. {0}", ex.Message);

                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
