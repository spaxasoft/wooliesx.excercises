using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using WooliesX.Excercises.Apps.AzFuncs.Models;
using System;
using System.Net;

namespace WooliesX.Excercises.Apps.AzFuncs
{
    public static class UserFunction
    {
        [FunctionName("User")]
        public static IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            try
            {
                var response = new UserResponseModel
                {
                    Name = Environment.GetEnvironmentVariable("User"),
                    Token = Environment.GetEnvironmentVariable("Token")
                }; ;

                return new OkObjectResult(response);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "An error occured in /User function. {0}", ex.Message);

                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
