using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Linq;
using System.Net;
using WooliesX.Excercises.Apps.Services;

namespace WooliesX.Excercises.Apps.AzFuncs
{
    public class ProductsSortFunction
    {
        private readonly IProductsSortService _productSortService;

        public ProductsSortFunction(IProductsSortService productSortService)
        {
            _productSortService = productSortService;
        }

        [FunctionName("Sort")]
        public async Task<IActionResult> Run
        (
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] HttpRequest req,
            ILogger log
        )
        {
            try
            {
                var sortOption = req.Query.ContainsKey("sortOption") && req.Query["sortOption"].Count == 1
                    ? req.Query["sortOption"].FirstOrDefault()?.ToLower() ?? ""
                    : "";

                if (!_productSortService.SupportedSortOptions.Contains(sortOption))
                {
                    var errorResponse = $"Invalid Sort Option. Valid values are {string.Join(", ", _productSortService.SupportedSortOptions)}";

                    log.LogError("Bad Request: {0}", errorResponse);

                    return new BadRequestObjectResult(errorResponse);
                }

                var requestBaseUrl = Environment.GetEnvironmentVariable("WooliesXApiBaseUrl");
                var token = Environment.GetEnvironmentVariable("Token");

                var products = await _productSortService.GetSortedProductsAsync(requestBaseUrl, token, sortOption);

                return new OkObjectResult(products);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "An error occured in /Sort function. {0}", ex.Message);

                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
