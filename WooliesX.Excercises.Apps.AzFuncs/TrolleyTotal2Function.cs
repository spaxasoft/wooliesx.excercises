using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net;
using WooliesX.Excercises.Apps.Services;
using WooliesX.Excercises.Apps.Services.Serialization;
using WooliesX.Excercises.Apps.Services.Models;

namespace WooliesX.Excercises.Apps.AzFuncs
{
    public class TrolleyTotal2Function
    {
        private readonly ITrolleyService _trolleyService;

        public TrolleyTotal2Function(ITrolleyService trolleyService)
        {
            _trolleyService = trolleyService;
        }

        [FunctionName("TrolleyTotal2")]
        public IActionResult Run
        (
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            ILogger log
        )
        {
            try
            {
                var model = FastJsonSerializer.Deserialize<TrolleyModel>(req.Body);

                if (model == null || model.Quantities == null)
                {
                    var errorResponse = "Invalid trolley request body";

                    log.LogError("Bad Request: {0}", errorResponse);

                    return new BadRequestObjectResult(errorResponse);
                }

                var trolleyTotal = _trolleyService.GetTrolleyTotal2(model);

                return new OkObjectResult(trolleyTotal);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "An error occured in /TrolleyTotal2 function. {0}", ex.Message);

                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
