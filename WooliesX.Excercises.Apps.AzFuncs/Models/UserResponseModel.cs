﻿namespace WooliesX.Excercises.Apps.AzFuncs.Models
{
    public class UserResponseModel
    {
        public string Name { get; set; }
        public string Token { get; set; }
    }
}
