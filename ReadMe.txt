Hi There,

I have finalised all 4 excercises.

https://bitbucket.org/spaxoft/wooliesx.excercises/src/develop/


Some thoughts to take into consideration.

1- I tried to keep the code and logics very simple tor review.

2- I could and wanted to bring database/EF/EF-Migrations in Azure SQL but I thought it wasn't required as it is all microservices and my Azure Functions did not need to interact with db
In regards to /User function since it was just a simple call without any sort of authentication around, I did not create any database for it (just 1 user without any credential checking).

3- It's advisable to always have some sort of authentication such as OAuth2 unless those are all internal IPs. Again I kept it simple.

4- In regards to the excercise4 I have done it as you can have a look at source control and it is accessible at .../TrolleyTotal2 url.

6- Unit tests are based on XUnit and Moq. I just kept it simple (only Azure Function and not full coverage) for the sake of easy review on your side but ideally we should have unit test for Services, DataModels/Repositories/UnitOfWork/...

8- Apart from local.settings.json, I ignored setting any config file locally for other environments (just development for now) because of the demo. Using CI/CD we can achieve auto deplotments on different environments.

9- I did not use AutoMapper or any other 3rdparty framework. Just pure .NET Frameworks and Moq for mocking. 

10- Every changeset is a separate commit in it's own branch that has been merged into develop branch by creating a pull request (by myself) and merging (by myself :) obviously because I was only developer for that) to give you easier review process if you would like to see what happened in each commit.

 

Finally I appreciate you for giving me the opportunity to prove my capabilities by providing this test challenge, I am happy as always because again I learned more.

I have a lot of passion and knowledge worth of over 15 years experience to bring to the team and always know that I will learn a lot from the team too.

 

Regards
Saeid

 