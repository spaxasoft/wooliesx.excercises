using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Enums;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Factories;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Logging;
using WooliesX.Excercises.Apps.AzFuncs;
using WooliesX.Excercises.Apps.Services;
using WooliesX.Excercises.Apps.Services.Models;
using Xunit;
using System.Net.Http.Json;

namespace WooliesX.Excercise.Apps.AzFuncs.Tests
{
    public class TrolleyCalculatorFunctionTests
    {
        private ListLogger logger;
        private Mock<ITrolleyService> mockTrolleyService;
        private TrolleyTotalFunction trolleyCalculatorFunction;

        public TrolleyCalculatorFunctionTests()
        {
            logger = (ListLogger)TestFactory.CreateLogger(LoggerTypes.List);

            mockTrolleyService = new Mock<ITrolleyService>();

            trolleyCalculatorFunction = new TrolleyTotalFunction(mockTrolleyService.Object);
        }

        [Fact]
        public async Task Should_return_bad_request_if_no_valid_request_body()
        {
            var request = TestFactory.CreateHttpRequest();
            request.Body = await JsonContent.Create(new { X = 1, Y = 2 }).ReadAsStreamAsync();

            var response = await trolleyCalculatorFunction.Run(request, logger);

            Assert.IsType<BadRequestObjectResult>(response);
            mockTrolleyService.Verify(x => x.GetTrolleyTotalAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<TrolleyModel>()), Times.Never);
        }

        [Fact]
        public async Task UserFunction_should_return_200_if_succeeded()
        {
            var wooliesXApiBaseUrl = "xyz-test.com";
            var token = "111-222-333-444-555";
            var trolleyModel = new TrolleyModel
            {
                Products = new TrolleyProductPriceModel[]
                    {
                        new TrolleyProductPriceModel{ Name="P1", Price = 10 },
                        new TrolleyProductPriceModel{ Name="P2", Price = 30 }
                    },
                Specials = new TrolleySpecialModel[]
                    {
                        new TrolleySpecialModel
                        {
                            Quantities = new TrolleyProductQuantityModel[]
                            {
                                new TrolleyProductQuantityModel{ Name="P1", Quantity = 4 }
                            },
                            Total = 5
                        }
                    },
                Quantities = new TrolleyProductQuantityModel[]
                    {
                        new TrolleyProductQuantityModel{ Name = "P1", Quantity = 8 },
                        new TrolleyProductQuantityModel{ Name = "P2", Quantity = 5 }
                    }
            };

            var request = TestFactory.CreateHttpRequest();
            request.Body = await JsonContent.Create(trolleyModel).ReadAsStreamAsync();

            Environment.SetEnvironmentVariable("WooliesXApiBaseUrl", wooliesXApiBaseUrl);
            Environment.SetEnvironmentVariable("Token", token);

            mockTrolleyService
                .Setup(x => x.GetTrolleyTotalAsync(wooliesXApiBaseUrl, token, It.IsAny<TrolleyModel>()))
                .Returns(Task.FromResult(105M));

            var response = await trolleyCalculatorFunction.Run(request, logger);

            var result = Assert.IsType<OkObjectResult>(response);
            mockTrolleyService.Verify(x => x.GetTrolleyTotalAsync(wooliesXApiBaseUrl, token, It.IsAny<TrolleyModel>()), Times.Once);

            var value = Assert.IsType<decimal>(result.Value);
            Assert.Equal(105M, value);
        }

        //And a lot more to test everything
    }
}
