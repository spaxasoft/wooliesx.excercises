﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Text;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Enums;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Logging;

namespace WooliesX.Excercise.Apps.AzFuncs.Tests.Factories
{
    public class TestFactory
    {
        public static HttpRequest CreateHttpRequest(Dictionary<string, StringValues> queryStringParams = null)
        {
            var context = new DefaultHttpContext();
            var request = context.Request;
            request.Query = queryStringParams == null
                ? new QueryCollection()
                : new QueryCollection(queryStringParams);
            return request;
        }

        public static ILogger CreateLogger(LoggerTypes type = LoggerTypes.Null)
        {
            ILogger logger;

            if (type == LoggerTypes.List)
            {
                logger = new ListLogger();
            }
            else
            {               
                logger = NullLoggerFactory.Instance.CreateLogger("Null Logger");
            }

            return logger;
        }
    }
}
