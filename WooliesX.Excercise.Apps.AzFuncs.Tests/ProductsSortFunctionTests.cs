using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Moq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Enums;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Factories;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Logging;
using WooliesX.Excercises.Apps.AzFuncs;
using WooliesX.Excercises.Apps.Services;
using WooliesX.Excercises.Apps.Services.Models;
using Xunit;

namespace WooliesX.Excercise.Apps.AzFuncs.Tests
{
    public class ProductsSortFunctionTests
    {
        private ListLogger logger;
        private Mock<IProductsSortService> mockProductSortService;
        private ProductsSortFunction productsSortFunction;

        public ProductsSortFunctionTests()
        {
            logger = (ListLogger)TestFactory.CreateLogger(LoggerTypes.List);

            mockProductSortService = new Mock<IProductsSortService>();
            mockProductSortService
                .SetupGet(x => x.SupportedSortOptions)
                .Returns(new[]
                {
                    "low",
                    "high",
                    "ascending",
                    "descending",
                    "recommended"
                }
            );

            productsSortFunction = new ProductsSortFunction(mockProductSortService.Object);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("xyz")]
        public async Task Should_return_bad_request_if_no_valid_sort_option_query_parameter(string sortOption)
        {
            var queryStringParams = sortOption == null
                ? null
                : new Dictionary<string, StringValues>
                {
                    ["sortOption"] = sortOption
                };

            var request = TestFactory.CreateHttpRequest(queryStringParams);

            var response = await productsSortFunction.Run(request, logger);

            Assert.IsType<BadRequestObjectResult>(response);
            mockProductSortService.Verify(x => x.GetSortedProductsAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task UserFunction_should_return_200_if_succeeded()
        {
            var queryStringParams = new Dictionary<string, StringValues>
            {
                ["sortOption"] = "Low"
            };

            var request = TestFactory.CreateHttpRequest(queryStringParams);

            var wooliesXApiBaseUrl = "xyz-test.com";
            var token = "111-222-333-444-555";
            var sortOption = "Low";

            Environment.SetEnvironmentVariable("WooliesXApiBaseUrl", wooliesXApiBaseUrl);
            Environment.SetEnvironmentVariable("Token", token);

            mockProductSortService
                .Setup(x => x.GetSortedProductsAsync(wooliesXApiBaseUrl, token, sortOption.ToLower()))
                .Returns(Task.FromResult(new List<ProductModel>
                {
                    new ProductModel{ Name="P1", Price=10, Quantity=45 },
                    new ProductModel{ Name="P2", Price=20, Quantity=10 },
                    new ProductModel{ Name="P3", Price=30, Quantity=15 }
                }
                .AsEnumerable()));

            var response = await productsSortFunction.Run(request, logger);

            var result = Assert.IsType<OkObjectResult>(response);
            mockProductSortService.Verify(x => x.GetSortedProductsAsync(wooliesXApiBaseUrl, token, sortOption.ToLower()), Times.Once);
        }

        //And a lot more to test everything
    }
}
