﻿using System;

namespace WooliesX.Excercise.Apps.AzFuncs.Tests.Logging
{
    public class NullScope : IDisposable
    {
        public static NullScope Instance { get; } = new NullScope();

        private NullScope() { }

        public void Dispose() { }
    }
}
