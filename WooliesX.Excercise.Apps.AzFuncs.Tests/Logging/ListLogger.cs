﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace WooliesX.Excercise.Apps.AzFuncs.Tests.Logging
{
    public class ListLogger : ILogger
    {
        public List<string> Logs;

        public IDisposable BeginScope<TState>(TState state) => NullScope.Instance;

        public bool IsEnabled(LogLevel logLevel) => false;

        public ListLogger()
        {
            Logs = new List<string>();
        }

        public void Log<TState>(LogLevel logLevel,
                                EventId eventId,
                                TState state,
                                Exception exception,
                                Func<TState, Exception, string> formatter)
        {
            string message = formatter(state, exception);
            this.Logs.Add(message);
        }
    }
}
