using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Enums;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Factories;
using WooliesX.Excercise.Apps.AzFuncs.Tests.Logging;
using WooliesX.Excercises.Apps.AzFuncs;
using WooliesX.Excercises.Apps.AzFuncs.Models;
using Xunit;

namespace WooliesX.Excercise.Apps.AzFuncs.Tests
{
    public class UserFunctionTests
    {
        [Fact]
        public void UserFunction_should_return_200_if_succeeded()
        {
            var request = TestFactory.CreateHttpRequest();
            var logger = TestFactory.CreateLogger(LoggerTypes.List);

            var response = UserFunction.Run(request, logger);

            Assert.IsType<OkObjectResult>(response);
        }

        [Fact]
        public void UserFunction_should_return_result_if_succeeded()
        {
            var request = TestFactory.CreateHttpRequest();
            var logger = (ListLogger)TestFactory.CreateLogger(LoggerTypes.List);

            Environment.SetEnvironmentVariable("User", "Me");
            Environment.SetEnvironmentVariable("Token", "111-222-333-444-555");

            var response = UserFunction.Run(request, logger);

            var result = Assert.IsType<OkObjectResult>(response);

            var model = Assert.IsType<UserResponseModel>(result.Value);

            Assert.Equal("Me", model.Name);
            Assert.Equal("111-222-333-444-555", model.Token);
        }
    }
}
